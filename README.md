# KIBI ⌨️

**KIBI** ⌨️ is a simple typing test web application. Built with [**React**](https://react.dev/) ⚛️ and [**Spring Boot**](https://spring.io/projects/spring-boot) 🍃. **KIBI** ⌨️ is live on [kibi.fuadismail.com](http://kibi.fuadismail.com).

<img src=https://github.com/fuad1502/kibi/blob/master/doc/kibi.gif width=500px max-width=100%>

This repository contains the front-end source. Refer to [kibi-spring](https://github.com/fuad1502/kibi-spring) for the backend.

## Words dataset

The words are randomly selected from the [English Word Frequency](https://www.kaggle.com/datasets/rtatman/english-word-frequency) dataset weighted by it's frequency.

## Build & Deploy

### Prerequisites

- [**Podman**]() 🦭 / [**Docker**]() 🐋

The following steps assumes you're using **Podman**. If you want to use **Docker** instead, simply change every `docker` keyword with `podman`.

### Steps

```
git clone https://github.com/fuad1502/kibi.git
git clone https://github.com/fuad1502/kibi-spring.git
```

Edit `REACT_APP_API_ADDR` in the `.env` file inside `kibi/` according to the URL where you want to deploy your backend. In the following example, we assume you deploy it to `http://localhost:8080`.

Next, set the following environment variables according to your deployment environment. For example, in Linux:

```sh
export KIBI_IP_ADDR=127.0.0.1
export KIBI_PORT=80
export KIBI_HOSTNAME=http://localhost
export KIBISPRING_IP_ADDR=127.0.0.1
export KIBISPRING_PORT=8080
```

```sh
cd kibi
podman build -t kibi .
podman run -d -p $KIBI_IP_ADDR:$KIBI_PORT:3000 kibi
cd ../kibi-spring
podman build -t kibi-spring .
podman run -d -p $KIBISPRING_IP_ADDR:$KIBISPRING_PORT:8080 -e CORS_ORIGIN=$KIBI_HOSTNAME kibi-spring
```
