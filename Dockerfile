FROM node AS build
WORKDIR /app
COPY package.json .
RUN npm install
COPY .env .
COPY src src
COPY public public
RUN npm run build

FROM node AS run
RUN npm install -g serve
WORKDIR /app
COPY --from=build /app/build build
ENTRYPOINT ["serve", "-s", "build"]
